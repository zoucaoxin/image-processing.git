package com.example.imageprocessing.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imageprocessing.R;

import cn.walkpast.stamperlib.StampPadding;
import cn.walkpast.stamperlib.StampType;
import cn.walkpast.stamperlib.StampWatcher;
import cn.walkpast.stamperlib.Stamper;

public class AddWatermarkActivity extends Activity implements View.OnClickListener {

    private TextView mBtnImage;
    private TextView mBtnText;
    private ImageView mShowImage;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_watermark);

        mBtnImage = findViewById(R.id.btn_image);
        mBtnText = findViewById(R.id.btn_text);
        mShowImage = findViewById(R.id.show_image);

        mBtnImage.setOnClickListener(this);
        mBtnText.setOnClickListener(this);

        String imgUrl = getIntent().getStringExtra("imgUrl");
        bitmap = BitmapFactory.decodeFile(imgUrl);
        mShowImage.setImageBitmap(bitmap);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_text:

                Stamper.with(AddWatermarkActivity.this)
                        .setLabel("加个标签")
                        .setLabelColor(getResources().getColor(R.color.theme))
                        .setLabelSize(60)
                        .setMasterBitmap(bitmap)
                        .setStampType(StampType.TEXT)
                        .setStampPadding(new StampPadding(bitmap.getWidth() / 4, bitmap.getHeight() / 6))
                        .setStampWatcher(mStampWatcher)
                        .setRequestId(1001)
                        .build();
                break;

            case R.id.btn_image:

                Bitmap watermark = BitmapFactory.decodeResource(getResources(), R.drawable.ic_watermark);
                Stamper.with(AddWatermarkActivity.this)
                        .setMasterBitmap(bitmap)
                        .setWatermark(watermark)
                        .setStampType(StampType.IMAGE)
                        .setStampPadding(new StampPadding(bitmap.getWidth() - watermark.getWidth() - 40, 40))
                        .setStampWatcher(mStampWatcher)
                        .setRequestId(1002)
                        .build();

                break;
        }
    }


    StampWatcher mStampWatcher = new StampWatcher() {
        @Override
        protected void onSuccess(Bitmap bitmap, int requestId) {
            super.onSuccess(bitmap, requestId);

            switch (requestId) {

                case 1001:
                    //the result of text stamper

                    mShowImage.setImageBitmap(bitmap);

                    break;
                case 1002:
                    //the result of image stamper

                    mShowImage.setImageBitmap(bitmap);

                    break;
            }
        }

        @Override
        protected void onError(String error, int requestId) {
            super.onError(error, requestId);

            switch (requestId) {

                case 1001://

                    Toast.makeText(AddWatermarkActivity.this, "error:" + error, Toast.LENGTH_SHORT).show();

                    break;
                case 1002://

                    Toast.makeText(AddWatermarkActivity.this, "error:" + error, Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    };


}
