package com.example.imageprocessing.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.imageprocessing.R;

public class TuPianXiaoGuo extends AppCompatActivity {

    private ImageView mImageView;
    private Bitmap bitmap;
    private String imgUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tupianxiaoguo);

        mImageView = findViewById(R.id.imageView);

        imgUrl = getIntent().getStringExtra("imgUrl");
        bitmap = BitmapFactory.decodeFile(imgUrl);
        mImageView.setImageBitmap(bitmap);
        ImageHelper.compressImage(bitmap);

    }

    public void btn_negative(View view) {
        bitmap = BitmapFactory.decodeFile(imgUrl);
        mImageView.setImageBitmap(bitmap);
        ImageHelper.compressImage(bitmap);
        getNegative();
    }

    public void btn_reminiscence(View view) {
        bitmap = BitmapFactory.decodeFile(imgUrl);
        mImageView.setImageBitmap(bitmap);
        ImageHelper.compressImage(bitmap);
        getReminiscence();
    }

    public void btn_emboss(View view) {
        bitmap = BitmapFactory.decodeFile(imgUrl);
        mImageView.setImageBitmap(bitmap);
        ImageHelper.compressImage(bitmap);
        getEmboss();
    }

    public void btn_black(View view) {
        bitmap = BitmapFactory.decodeFile(imgUrl);
        ImageHelper.compressImage(bitmap);
        getBlack();
    }

    /**
     * 黑白效果
     */
    private void getBlack() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap base = Bitmap.createBitmap(width, height, bitmap.getConfig());

        Canvas canvas = new Canvas(base);
        canvas.drawBitmap(bitmap, new Matrix(), new Paint());

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = bitmap.getPixel(i, j);

                int r = Color.red(color);
                int g = Color.green(color);
                int b = Color.blue(color);
                int a = Color.alpha(color);

                int avg = (r + g + b) / 3;

                if (avg >= 100) {
                    //设为白色
                    base.setPixel(i, j, Color.argb(a, 255, 255, 255));
                } else {
                    //设为黑色
                    base.setPixel(i, j, Color.argb(a, 0, 0, 0));
                }
            }
        }
        bitmap.recycle();
        mImageView.setImageBitmap(base);
    }

    /**
     * 浮雕
     */
    private void getEmboss() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap base = Bitmap.createBitmap(width, height, bitmap.getConfig());

        Canvas canvas = new Canvas(base);
        canvas.drawBitmap(bitmap, new Matrix(), new Paint());

        int pre_color = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int current_color = bitmap.getPixel(i, j);

                int r = Color.red(current_color) - Color.red(pre_color) + 128;
                int g = Color.green(current_color) - Color.green(pre_color) + 128;
                int b = Color.blue(current_color) - Color.blue(pre_color) + 128;
                int a = Color.alpha(current_color);

                int newcolor = (int) (r * 0.3 + g * 0.59 + b * 0.11);

                base.setPixel(i, j, Color.argb(a, newcolor, newcolor, newcolor));
                pre_color = current_color;
            }
        }
        bitmap.recycle();
        mImageView.setImageBitmap(base);
    }

    /**
     * 怀旧风格
     */
    private void getReminiscence() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap base = Bitmap.createBitmap(width, height, bitmap.getConfig());

        Canvas canvas = new Canvas(base);
        canvas.drawBitmap(bitmap, new Matrix(), new Paint());

        int newR, newG, newB;
        for (int i = 0; i < width; i++)//遍历像素点
        {
            for (int j = 0; j < height; j++) {
                int current_color = bitmap.getPixel(i, j);
                int r = Color.red(current_color);
                int g = Color.green(current_color);
                int b = Color.blue(current_color);
                int a = Color.alpha(current_color);

                /*经验公式*/
                newR = (int) (0.393 * r + 0.769 * g + 0.189 * b);
                newG = (int) (0.349 * r + 0.686 * g + 0.168 * b);
                newB = (int) (0.272 * r + 0.534 * g + 0.131 * b);

                int newColor = Color.argb(a, newR > 255 ? 255 : newR, newG > 255 ? 255 : newG, newB > 255 ? 255 : newB);
                base.setPixel(i, j, newColor);
            }
        }
        bitmap.recycle();
        mImageView.setImageBitmap(base);
    }

    /**
     * 底片效果
     */
    private void getNegative() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap base = Bitmap.createBitmap(width, height, bitmap.getConfig());
        Paint paint = new Paint();

        Canvas canvas = new Canvas(base);
        canvas.drawBitmap(bitmap, new Matrix(), paint);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = bitmap.getPixel(i, j);
                int r = Color.red(color);
                int g = Color.green(color);
                int b = Color.blue(color);
                int a = Color.alpha(color);
                base.setPixel(i, j, Color.argb(a, 255 - r, 255 - g, 255 - b));
            }
        }
        bitmap.recycle();
        mImageView.setImageBitmap(base);
    }

}
