package com.example.imageprocessing.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

import com.example.imageprocessing.R;

public class PrimaryColorActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private ImageView mImageView;

    private SeekBar mSeekBarHue, mSeekBarSaturation, mSeekBarLum;

    private static int MAX_VALUE = 255;

    private static int MID_VALUE = 127;

    private float mHue, mSaturation, mLum;

    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary_color);

        String imgUrl = getIntent().getStringExtra("imgUrl");
        bitmap = BitmapFactory.decodeFile(imgUrl);

        mImageView = findViewById(R.id.imageView);
        mSeekBarHue = findViewById(R.id.bar_hue);
        mSeekBarSaturation = findViewById(R.id.bar_saturation);
        mSeekBarLum = findViewById(R.id.bar_lum);

        mSeekBarHue.setOnSeekBarChangeListener(this);
        mSeekBarSaturation.setOnSeekBarChangeListener(this);
        mSeekBarLum.setOnSeekBarChangeListener(this);

        mSeekBarHue.setMax(MAX_VALUE);
        mSeekBarSaturation.setMax(MAX_VALUE);
        mSeekBarLum.setMax(MAX_VALUE);

        mSeekBarHue.setProgress(MID_VALUE);
        mSeekBarSaturation.setProgress(MID_VALUE);
        mSeekBarLum.setProgress(MID_VALUE);

        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.bar_hue:
                mHue = (progress - MID_VALUE) * 1.0F / MID_VALUE * 180;
                break;
            case R.id.bar_saturation:
                mSaturation = progress * 1.0F / MID_VALUE;
                break;
            case R.id.bar_lum:
                mLum = progress * 1.0F / MID_VALUE;
                break;
        }

        mImageView.setImageBitmap(ImageHelper.handleImageEffect(bitmap, mHue, mSaturation, mLum));


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
